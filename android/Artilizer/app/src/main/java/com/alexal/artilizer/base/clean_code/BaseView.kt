package com.alexal.artilizer.base.clean_code

import android.content.Context
import android.view.View
import android.view.ViewGroup

/**
 *
 * @author alexal
 */
abstract class BaseView<P: BaseContract.Presenter<BaseContract.View>>(context: Context?): BaseContract.View, ViewGroup(context){

  lateinit var presenter: P

  init {
    initViews()
    presenter.attachView(this)
  }

  protected abstract fun initViews()
  protected abstract fun getLayoutId(): Int

  fun injectPresenter(presenter: P){
    this.presenter = presenter
  }

  override fun onDetachedFromWindow() {
    super.onDetachedFromWindow()
    presenter.detachView()
  }
}