package com.alexal.artilizer.domain.remote

import com.alexal.artilizer.domain.local.PictureResult
import java.util.*
import java.util.concurrent.Executor

/**
 *
 * @author alexal
 */
class ArtifyServer(private val configuration: Configuration){

  class Configuration(val queriesExecutionThread: Executor,
                      val endPoint: String)

  companion object {
    val ENDPOINT = "ADD_ENDPOINT_HERE"
    val IMAGE_KEY = "image"
    val DELAY_SIMULATION = 10000L
  }

  fun sendImage(image: ByteArray,
                callback: (pictureResult: PictureResult) -> Unit){
    configuration.queriesExecutionThread.execute {
      // TODO

      Thread.sleep(DELAY_SIMULATION)

      val pictureResult = PictureResult(10, image, "MY_ART", Calendar.getInstance().time)

      callback.invoke(pictureResult)
    }
  }

}