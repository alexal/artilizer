package com.alexal.artilizer.base.utils.view

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View


/**
 *
 * @author alexal
 */
class SpacesItemDecoration(private val mSpace: Int) : RecyclerView.ItemDecoration() {

  override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
    outRect.left = mSpace
    outRect.right = mSpace
    outRect.bottom = mSpace

    // Add top margin only for the first item to avoid double space between items
    if (parent.getChildAdapterPosition(view) == 0)
      outRect.top = mSpace
  }
}