package com.alexal.artilizer.base.clean_code

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.os.Bundle


/**
 *
 * Created by alexal on 2/10/2018.
 */
abstract class BasePresenter<V: BaseContract.View>: LifecycleObserver,BaseContract.Presenter<V>{

  private var view: V? = null
  private var stateBundle = Bundle()

  override fun getView(): V {
    return view!!
  }

  override fun attachView(view: V) {
    this.view = view
    onViewAttached()
  }

  override fun detachView() {
    view = null
  }

  override fun isViewAttached(): Boolean {
    return view != null
  }

  fun getStateBundle(): Bundle{
    return stateBundle
  }

  /**
   * This is the entry point of the presenter.
   */
  abstract fun onViewAttached()

}