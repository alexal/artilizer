package com.alexal.artilizer.home.view.picture_screen

import com.alexal.artilizer.base.clean_code.BasePresenter
import com.alexal.artilizer.home.view.HomeContract

/**
 *
 * @author alexal
 */
class ResultsPresenter: BasePresenter<HomeContract.PictureResultView>(), HomeContract.ResultPresenter {
  override fun onViewAttached() {
  }

  override fun onShareClick() {
  }

  override fun onStoreClick() {
  }

}