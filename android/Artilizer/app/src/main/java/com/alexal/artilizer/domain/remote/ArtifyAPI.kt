package com.alexal.artilizer.domain.remote

import com.alexal.artilizer.base.clean_code.AppExecutors
import com.alexal.artilizer.domain.local.PictureResult

/**
 *
 * @author alexal
 */
object ArtifyAPI{

  private val server: ArtifyServer

  init {
    val serverConfiguration = ArtifyServer.Configuration(AppExecutors.networkIO, "END_POINT")
    server = ArtifyServer(serverConfiguration)
  }

  fun serverIsOk(): Boolean{
    // TODO
    return true
  }

  fun processImageWithEffect(effectId: Long,
                             imageByteArray: ByteArray,
                             callback: (pictureResult: PictureResult) -> Unit){
    // TODO
    server.sendImage(imageByteArray) {callback.invoke(it)}
  }

  fun fetchAvailableEffects(callback: (effects: Effect) -> Unit){
    // TODO
  }

}