package com.alexal.artilizer.domain.local.effects_repository

import com.alexal.artilizer.R

/**
 *
 * @author alexal
 */
class LocalArtEffect(val id: Long, val name: String, val resourceId: Int)

/**
 * The repository of the effects. Currently they are stored locally, maybe later
 * the implementation changes to remote.
 * @author alexal
 */
object EffectsRepository{

  fun fetchAllEffects(callback: (effects: List<LocalArtEffect>) -> Unit){

    val ef1 = LocalArtEffect(1,"Rick and morty", R.drawable.rick_and_mort_resize)
    val result = ArrayList<LocalArtEffect>()
    result.add(ef1)
    result.add(ef1)
    result.add(ef1)
    result.add(ef1)
    result.add(ef1)
    result.add(ef1)
    result.add(ef1)
    result.add(ef1)
    result.add(ef1)
    result.add(ef1)
    result.add(ef1)
    result.add(ef1)
    result.add(ef1)
    result.add(ef1)
    result.add(ef1)
    result.add(ef1)
    result.add(ef1)
    result.add(ef1)

    callback.invoke(result)
  }

}