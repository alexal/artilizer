package com.alexal.artilizer.home.interactors

import android.Manifest
import android.content.Context
import com.alexal.artilizer.base.clean_code.AbstractInteractor
import com.alexal.artilizer.base.utils.App
import com.alexal.artilizer.home.view.HomeContract
import java.util.concurrent.Executor

/**
 *
 * @author alexal
 */
class PermissionsInteractor(executionThread: Executor,
                            mainThread: Executor,
                            private val homeView: HomeContract.PictureCaptureView,
                            private val context: Context) : AbstractInteractor(executionThread, mainThread) {

  override fun run() {

    // Check for permissions

    if ( !App.hasPermissions(context, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE) ){
      homeView.askForPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }

  }

}