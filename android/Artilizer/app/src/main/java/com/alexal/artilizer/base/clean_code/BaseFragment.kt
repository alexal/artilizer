package com.alexal.artilizer.base.clean_code

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Base fragment class to use. If you use MVP and do not prefer custom views as the View layer
 * you can use fragments instead with all the fragment functionality.
 * @author alexal
 */
abstract class BaseFragment<V: BaseContract.View,P: BasePresenter<V>>: Fragment(), BaseContract.View {

  protected lateinit var presenter: P

  fun injectPresenter(presenter: P){
    this.presenter = presenter
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    return inflater.inflate(getLayoutId(), container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    presenter.attachView(getThisView())
    initViews()
  }

  override fun onDestroy() {
    super.onDestroy()
    presenter.detachView()
  }

  protected abstract fun initViews()

  protected abstract fun getThisView(): V

  protected abstract fun getLayoutId(): Int
}