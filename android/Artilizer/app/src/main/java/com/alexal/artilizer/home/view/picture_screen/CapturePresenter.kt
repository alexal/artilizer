package com.alexal.artilizer.home.view.picture_screen

import android.graphics.Bitmap
import android.net.Uri
import com.alexal.artilizer.base.clean_code.BasePresenter
import com.alexal.artilizer.home.interactors.PermissionsInteractor
import com.alexal.artilizer.home.interactors.SelectGalleryInteractor
import com.alexal.artilizer.home.interactors.TakePictureInteractor
import com.alexal.artilizer.home.view.HomeContract

/**
 *
 * @author alexal
 */
class CapturePresenter(private val permissionsInteractor: PermissionsInteractor,
                       private val galleryInteractor: SelectGalleryInteractor,
                       private val takePictureInteractor: TakePictureInteractor): BasePresenter<HomeContract.PictureCaptureView>(),HomeContract.CapturePresenter {

  override fun onGalleryClick() {
    galleryInteractor.execute()
  }

  override fun onTakePictureClick() {
    takePictureInteractor.execute()
  }

  override fun onViewAttached() {
    permissionsInteractor.execute()
  }

  override fun onPictureSelected(imageUri: Uri) {
    getView().showMessage("Picture selected gallery")
  }

  override fun onPictureSelected(imageBitmap: Bitmap) {
    getView().showMessage("Picture selected bitmap")
  }

}