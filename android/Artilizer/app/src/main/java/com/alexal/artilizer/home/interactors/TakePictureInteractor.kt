package com.alexal.artilizer.home.interactors

import com.alexal.artilizer.base.clean_code.AbstractInteractor
import com.alexal.artilizer.home.view.HomeContract
import java.util.concurrent.Executor

/**
 *
 * @author alexal
 */
class TakePictureInteractor(exThread: Executor,
                            val uiThtread: Executor,
                            private val captureView: HomeContract.PictureCaptureView): AbstractInteractor(exThread,uiThtread){
  override fun run() {
    uiThtread.execute { captureView.requestSystemCameraSelection() }
  }
}
