package com.alexal.artilizer.base.clean_code

import android.support.v7.app.AppCompatActivity
import android.support.annotation.CallSuper
import android.arch.lifecycle.LifecycleRegistry
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.annotation.LayoutRes


/**
 *
 * Created by alexal on 2/10/2018.
 */
abstract class BaseActivity: AppCompatActivity(),
  BaseContract.View {

  @CallSuper
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(getLayoutId())
//    val viewModel:BaseViewModel<V,P> = ViewModelProviders.of(this).get(BaseViewModel::class.java) as BaseViewModel<V, P>
//    if (viewModel.getPresenter() == null) {
//      viewModel.setPresenter(initPresenter())
//    }
//    presenter = viewModel.getPresenter()
//    presenter!!.attachLifecycle(lifecycle)
//    presenter!!.attachView(this as V)
    initViewHandles()
  }

  @LayoutRes
  protected abstract fun getLayoutId(): Int

  protected abstract fun initViewHandles()
}