package com.alexal.artilizer.home.view.picture_screen

import com.alexal.artilizer.base.clean_code.BasePresenter
import com.alexal.artilizer.home.view.HomeContract

/**
 *
 * @author alexal
 */
class EffectsPresenter: BasePresenter<HomeContract.PictureEffectsView>(), HomeContract.EffectsPresenter {

  override fun onViewAttached() {
  }

  override fun onEffectSelected(effectViewModel: HomeContract.EffectViewModel) {
  }

}