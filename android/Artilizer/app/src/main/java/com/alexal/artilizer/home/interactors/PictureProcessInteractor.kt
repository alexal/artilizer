package com.alexal.artilizer.home.interactors

import android.content.ContentResolver
import android.graphics.Bitmap
import android.net.Uri
import com.alexal.artilizer.base.clean_code.AbstractInteractor
import com.alexal.artilizer.domain.remote.ArtifyAPI
import com.alexal.artilizer.home.view.HomeContract
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.lang.IndexOutOfBoundsException
import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 *
 * @author alexal
 */
class PictureProcessInteractor(private val execThread: Executor,
                               private val uiThread: Executor,
                               private val pictureResultView: HomeContract.PictureResultView,
                               var imageUri: Uri?,
                               var imageBitmap: Bitmap?,
                               private val contentResolver: ContentResolver,
                               var effectViewModel: HomeContract.EffectViewModel?,
                               private val artifyAPI: ArtifyAPI): AbstractInteractor(execThread, uiThread){

  companion object {
    val ERROR_URI_INVALID = "Image URI provided is invalid"
    val ERROR_IMAGE_STREAM_INVALID = "Failed to open image stream"
    val ERROR_SERVER = "Problem with server communication"
    val ERROR_INVALID_EFFECT = "Effect chosen is invalid"
  }

  private var animateArrows: Boolean = true

  // Public API ----------------------------------------------------------------------------------->

  fun onStoreImageClick(){
    // TODO
  }

  fun onShareImageClick(){
    // TODO
  }

  //----------- ----------------------------------------------------------------------------------->

  @Throws(IOException::class)
  private fun getBytes(inputStream: InputStream): ByteArray? {

    val bytesResult: ByteArray?
    val byteBuffer = ByteArrayOutputStream()
    val bufferSize = 1024
    val buffer = ByteArray(bufferSize)
    try {
      var len: Int
      while (inputStream.available() > 0 && inputStream.read(buffer) != -1) {
        len = inputStream.read(buffer)
        try {
          byteBuffer.write(buffer, 0, len)
        }
        catch (e: IndexOutOfBoundsException){
          break
        }
      }
      bytesResult = byteBuffer.toByteArray()
    } finally {
      // close the stream
      try {
        byteBuffer.close()
      } catch (ignored: IOException) { /* do nothing */
      }

    }
    return bytesResult
  }

  private fun dispatchImageStreamError(error: String){
    uiThread.execute {
      pictureResultView.showMessage(error)
      pictureResultView.setProgressText("Canceled")
      pictureResultView.setProgressVis(false)
    }
    Timber.e(error)
  }

  private fun animateArrows(){
    Executors.newSingleThreadExecutor().execute {
      var arrowIndex = 0
      var arrowHidden1Index = 1
      var arrowHidden2Index = 2

      while (animateArrows){

        Thread.sleep(1000L)

        if (animateArrows){ // Yes, possible to change in 1 sec so recheck
          uiThread.execute {
            pictureResultView.animateArrow(arrowIndex, true)
            pictureResultView.animateArrow(arrowHidden1Index, false)
            pictureResultView.animateArrow(arrowHidden2Index, false)
          }
          arrowIndex = (arrowIndex + 1) % 3
          arrowHidden1Index = ( arrowIndex + 1) % 3
          arrowHidden2Index = ( arrowIndex + 2) % 3
        }
      }
    }

    uiThread.execute {
      pictureResultView.animateArrow(0, false)
      pictureResultView.animateArrow(1, false)
      pictureResultView.animateArrow(2, false)
    }
  }

  private fun setProgressText(progress: String){
    uiThread.execute { pictureResultView.setProgressText(progress) }
  }

  override fun run() {

    if ( !artifyAPI.serverIsOk() ){
      dispatchImageStreamError(ERROR_SERVER)
    }

    if (imageUri == null && imageBitmap == null){
      dispatchImageStreamError(ERROR_URI_INVALID)
      return
    }

    if (effectViewModel == null){
      dispatchImageStreamError(ERROR_INVALID_EFFECT)
      return
    }

    animateArrows()

    // Show progress text, animate progress and load the images
    uiThread.execute {
      pictureResultView.renderEffect(effectViewModel!!)

      if (imageUri != null)
        pictureResultView.renderPicture(imageUri!!)
      else
        pictureResultView.renderPicture(imageBitmap!!)

      pictureResultView.setProgressText("Optimizing image")
    }

    var imageBytes: ByteArray? = null

    // Dispatch image when URI is provided

    if (imageUri != null){

      val inputStream = contentResolver.openInputStream(imageUri)

      try {
        imageBytes = getBytes(inputStream)
      }
      catch (e: IOException){
        inputStream.close()
        dispatchImageStreamError(ERROR_IMAGE_STREAM_INVALID)
      }
      finally {
        inputStream.close()
      }

      if (imageBytes == null){
        dispatchImageStreamError(ERROR_IMAGE_STREAM_INVALID)
        return
      }
    }
    else{

      // Dispatch byte array from bitmap

      val stream = ByteArrayOutputStream()
      imageBitmap!!.compress(Bitmap.CompressFormat.PNG, 100, stream)
      imageBytes = stream.toByteArray()
//      imageBitmap!!.recycle()
    }

    setProgressText("Hell breaks loose in the server!")

    // Send the picture to the server and wait for the result
    artifyAPI.processImageWithEffect(effectViewModel!!.id, imageBytes!!) {

      val imageResult = it

      setProgressText("Ready! Enjoy art!")
      uiThread.execute {
        pictureResultView.renderPostProcessMenu()
        animateArrows = false
      }
      // Display the result
//      uiThread.execute { pictureResultView.renderPicture(imageResult.image) }

    }
  }

}