package com.alexal.artilizer.home.view.picture_screen

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.widget.Toast
import com.alexal.artilizer.R
import com.alexal.artilizer.base.clean_code.BaseFragment
import com.alexal.artilizer.home.view.HomeActivity
import com.alexal.artilizer.home.view.HomeContract
import kotlinx.android.synthetic.main.screen_home_picture_main.*

/**
 *
 * @author alexal
 */
class PictureCaptureView: BaseFragment<HomeContract.PictureCaptureView, CapturePresenter>(), HomeContract.PictureCaptureView {

  val GALLERY_KEY = 42
  val CAMERA_KEY = 43

  companion object {
    fun newInstance(): PictureCaptureView {

      val args = Bundle()

      val fragment = PictureCaptureView()
      fragment.arguments = args
      return fragment
    }
  }

  override fun askForPermissions(vararg permissions: String) {
    ActivityCompat.requestPermissions(requireActivity(),permissions,10)
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)

    if (requestCode == GALLERY_KEY){
      presenter.onPictureSelected(data?.data!!)
      return
    }

    if (requestCode == CAMERA_KEY){
      val imageBitmap = data!!.extras.get("data") as Bitmap
      presenter.onPictureSelected(imageBitmap)
      return
    }
  }

  override fun requestSystemGallerySelection() {

    val galleryIntent = Intent(
      Intent.ACTION_PICK,
      MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

    startActivityForResult(galleryIntent, GALLERY_KEY)

  }

  override fun requestSystemCameraSelection() {

    Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
      takePictureIntent.resolveActivity(requireActivity().packageManager)?.also {
        startActivityForResult(takePictureIntent, CAMERA_KEY)
      }
    }
  }

  override fun getThisView(): HomeContract.PictureCaptureView {
    return this
  }

  override fun showMessage(message: String) {
    Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
  }

  override fun getLayoutId(): Int {
    return R.layout.screen_home_picture_main
  }

  override fun initViews() {
    this.tv_home_picture.setOnClickListener { presenter.onTakePictureClick() }
    this.tv_home_gallery.setOnClickListener { presenter.onGalleryClick() }
  }

}