package com.alexal.artilizer.home.interactors

import android.net.Uri
import com.alexal.artilizer.base.clean_code.AbstractInteractor
import com.alexal.artilizer.domain.local.effects_repository.EffectsRepository
import com.alexal.artilizer.domain.local.effects_repository.LocalArtEffect
import com.alexal.artilizer.home.view.HomeContract
import java.util.concurrent.Executor

/**
 *
 * @author alexal
 */
class PicturePreviewInteractor(private val execThread: Executor,
                               private val uiThread: Executor,
                               var imageuri: Uri?,
                               private val effectsRepository: EffectsRepository,
                               private val previewView: HomeContract.PictureEffectsView): AbstractInteractor(execThread, uiThread){

  private fun parseToViewModel(artEffects: List<LocalArtEffect>): List<HomeContract.EffectViewModel>{
    return artEffects.map { localArtEffect -> HomeContract.EffectViewModel(localArtEffect.id,localArtEffect.name, localArtEffect.resourceId)}
  }

  override fun run() {

    // Show loading
    uiThread.execute { previewView.setLoadingVis(true) }

    // Get the effects
    effectsRepository.fetchAllEffects {

      Thread.sleep(3000L)

      // Parse them to view model

      val viewModels = parseToViewModel(it)

      // Hide loading and render them
      uiThread.execute {
        previewView.setLoadingVis(false)
        previewView.renderEffects(viewModels)
      }
    }
  }

}