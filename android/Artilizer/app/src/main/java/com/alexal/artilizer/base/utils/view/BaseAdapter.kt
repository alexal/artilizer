package com.alexal.artilizer.base.utils.view

import android.support.v7.widget.RecyclerView
import android.view.View

abstract class BaseAdapterModel

abstract class BaseAdapter<M: BaseAdapterModel,
                           V: BaseAdapter.BaseViewHolder>(private val data: MutableList<M>): RecyclerView.Adapter<V>() {

  override fun onBindViewHolder(holder: V, position: Int) {
    bind(holder, data[position])
  }

  override fun getItemCount(): Int {
    return data.size
  }

  abstract fun bind(holder: V, viewModel: M)

  open class BaseViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView)
}

