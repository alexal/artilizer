package com.alexal.artilizer.di

import android.content.Context
import com.alexal.artilizer.base.clean_code.AppExecutors
import com.alexal.artilizer.domain.local.effects_repository.EffectsRepository
import com.alexal.artilizer.domain.remote.ArtifyAPI
import com.alexal.artilizer.home.interactors.*
import com.alexal.artilizer.home.view.HomeContract
import com.alexal.artilizer.home.view.picture_screen.CapturePresenter

/**
 *
 * @author alexal
 */
class DIUtil {

  companion object {

    fun initPermissionsInteractor(capturePictureView: HomeContract.PictureCaptureView,
                                  context: Context): PermissionsInteractor{
      return PermissionsInteractor(
        AppExecutors.diskIO,
        AppExecutors.mainThread,
        capturePictureView,
        context)
    }

    fun initGalleryInteractor(capturePictureView: HomeContract.PictureCaptureView): SelectGalleryInteractor{
      return SelectGalleryInteractor(
        AppExecutors.diskIO,
        AppExecutors.mainThread,
        capturePictureView)
    }

    fun initTakePictureInteractor(capturePictureView: HomeContract.PictureCaptureView): TakePictureInteractor{
      return TakePictureInteractor(
        AppExecutors.diskIO,
        AppExecutors.mainThread,
        capturePictureView)
    }

//    fun initPictureProcessInteractor(pictureResultView: HomeContract.PictureResultView,
//                                     context: Context): PictureProcessInteractor{
//      return PictureProcessInteractor(
//        AppExecutors.networkIO,
//        AppExecutors.mainThread,
//        pictureResultView,
//        null,
//        null,
//        context.contentResolver,
//        null,
//        ArtifyAPI)
//    }
//
//    fun initPreviewInteractor(): PicturePreviewInteractor{
//      return PicturePreviewInteractor(
//        AppExecutors.diskIO,
//        AppExecutors.mainThread,
//        null,
//        EffectsRepository,
//        previewView)
//    }

    fun initCapturePresenter(capturePictureView: HomeContract.PictureCaptureView,
                             context: Context): CapturePresenter{
      return CapturePresenter(initPermissionsInteractor(capturePictureView, context),
        initGalleryInteractor(capturePictureView),
        initTakePictureInteractor(capturePictureView))
    }


  }
}