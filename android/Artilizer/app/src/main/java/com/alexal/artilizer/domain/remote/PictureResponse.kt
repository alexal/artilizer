package com.alexal.artilizer.domain.remote

import android.net.Uri

/**
 * TODO Configure with the server team
 * @author alexal
 */
data class PictureResponse(private val imageUri: Uri)