package com.alexal.artilizer.base.clean_code

/**
 * Created by alexal on 2/10/2018.
 */
interface Interactor:Runnable{
  fun execute()
}