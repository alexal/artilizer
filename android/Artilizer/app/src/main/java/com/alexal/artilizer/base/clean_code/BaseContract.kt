package com.alexal.artilizer.base.clean_code


/**
 *
 * Created by alexal on 2/10/2018.
 */
interface BaseContract{

  interface View {

  }

  interface Presenter<V: BaseContract.View> {

    fun attachView(view: V)

    fun detachView()

    fun getView(): V

    fun isViewAttached(): Boolean

  }

}