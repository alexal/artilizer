package com.alexal.artilizer.base.utils.view

import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.AlphaAnimation


/**
 *
 * @author alexal
 */
class AnimationUtil {

  companion object {

    fun animateBlink(imageView: View) {
      val animation = AlphaAnimation(1f, 0f)
      animation.duration = 700
      animation.interpolator = LinearInterpolator()
      animation.repeatCount = Animation.INFINITE
      animation.repeatMode = Animation.REVERSE
      imageView.visibility = View.VISIBLE
      imageView.startAnimation(animation)
    }

    fun stopBlinkAnimation(imageView: View) {
      imageView.clearAnimation()
    }

  }
}