package com.alexal.artilizer.base.clean_code

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 *
 * @author alexal
 */
object AppExecutors {

  val mainThread: Executor
  val networkIO: Executor
  val diskIO: Executor

  init {
    mainThread = MainThreadExecutor()
    networkIO = Executors.newFixedThreadPool(3)
    diskIO = Executors.newSingleThreadExecutor()
  }

  private class MainThreadExecutor : Executor {

    private val handler = Handler(Looper.getMainLooper())

    override fun execute(command: Runnable) {
      handler.post(command)
    }
  }
}