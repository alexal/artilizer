package com.alexal.artilizer.base.utils

import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat



/**
 *
 * @author alexal
 */
class App {
  companion object {
    fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
        for (permission in permissions) {
          if (ActivityCompat.checkSelfPermission(context!!, permission) != PackageManager.PERMISSION_GRANTED)
            return false
        }
      return true
    }
  }
}