package com.alexal.artilizer.home.interactors

import com.alexal.artilizer.base.clean_code.AbstractInteractor
import com.alexal.artilizer.domain.local.effects_repository.EffectsRepository
import com.alexal.artilizer.domain.local.effects_repository.LocalArtEffect
import com.alexal.artilizer.home.view.HomeContract
import java.util.concurrent.Executor

/**
 *
 * @author alexal
 */
class EffectsLoadInteractor(execThread: Executor,
                            val mainThread: Executor,
                            private val view: HomeContract.EffectsView,
                            private val effectsRepository: EffectsRepository): AbstractInteractor(execThread, mainThread){

  private fun parseToViewModel(artEffects: List<LocalArtEffect>): List<HomeContract.EffectViewModel>{
    return artEffects.map { localArtEffect -> HomeContract.EffectViewModel(localArtEffect.id,localArtEffect.name, localArtEffect.resourceId)}
  }

  override fun run() {

    // Fetch all the effects
    effectsRepository.fetchAllEffects {

      // Render them
      mainThread.execute { view.renderEffects(parseToViewModel(it)) }
    }
  }
}