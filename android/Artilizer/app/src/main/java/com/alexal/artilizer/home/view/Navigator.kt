package com.alexal.artilizer.home.view

/**
 *
 * @author alexal
 */
interface Navigator {
  fun navigateToCapturePictureScreen(): Boolean
  fun navigateToMyArtScreen(): Boolean
  fun navigateToEffectsScreen(): Boolean
  fun navigateToSettingsScreen(): Boolean
  fun navigateToPreview()
  fun navigateToPictureResultScreen()
}