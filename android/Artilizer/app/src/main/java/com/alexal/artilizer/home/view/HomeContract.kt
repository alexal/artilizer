package com.alexal.artilizer.home.view

import android.graphics.Bitmap
import android.net.Uri
import com.alexal.artilizer.base.clean_code.BaseContract
import com.alexal.artilizer.base.utils.view.BaseAdapterModel

/**
 *
 * Created by alexal on 2/10/2018.
 */
interface HomeContract:BaseContract{

  // Picture screen contract---------------------------------------------------------------------->

  interface PictureCaptureView:BaseContract.View {
    fun showMessage(message: String)
    fun askForPermissions(vararg permissions: String)
    fun requestSystemGallerySelection()
    fun requestSystemCameraSelection()
  }

  interface CapturePresenter:BaseContract.Presenter<PictureCaptureView> {
    fun onGalleryClick()
    fun onTakePictureClick()
    fun onPictureSelected(imageUri: Uri)
    fun onPictureSelected(imageBitmap: Bitmap)
  }

  class EffectViewModel(val id: Long, val name: String, val resourceId: Int): BaseAdapterModel()

  interface PictureEffectsView:BaseContract.View {
    fun renderEffects(effects: List<EffectViewModel>)
    fun setLoadingVis(shown: Boolean)
  }

  interface EffectsPresenter:BaseContract.Presenter<PictureEffectsView> {
    fun onEffectSelected(effectViewModel: EffectViewModel)
  }

  interface PictureResultView: BaseContract.View{
    fun renderEffect(effectViewModel: EffectViewModel)
    fun renderPicture(imageUri: Uri)
    fun renderPicture(imageByteArray: ByteArray)
    fun renderPicture(imageBitmap: Bitmap)
    fun animateArrow(arrowIndex: Int, animate: Boolean)
    fun setProgressText(text: String)
    fun setProgressVis(shown: Boolean)
    fun showMessage(message: String)
    fun renderPostProcessMenu()
  }

  interface ResultPresenter {
    fun onShareClick()
    fun onStoreClick()
  }

  // End of picture contracts---------------------------------------------------------------------->


  interface EffectsView: BaseContract.View {
    fun showLoading(shown: Boolean)
    fun renderEffects(effects: List<EffectViewModel>)
  }

  interface MyArtView:BaseContract.View

  interface SettingsView:BaseContract.View

  // Presenters layer ---------------------------------------------------------------------------->

  interface MyArtPresenter:BaseContract.Presenter<MyArtView>

  interface SettingsPresenter:BaseContract.Presenter<SettingsView>


}