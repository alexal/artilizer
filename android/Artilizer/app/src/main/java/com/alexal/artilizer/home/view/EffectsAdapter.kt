package com.alexal.artilizer.home.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.alexal.artilizer.R
import com.alexal.artilizer.base.utils.view.BaseAdapter
import com.bumptech.glide.Glide

/**
 *
 * @author alexal
 */
class EffectsAdapter(private val context: Context,
                     effects: MutableList<HomeContract.EffectViewModel>,
                     private val clickListener: ((effect: HomeContract.EffectViewModel) -> Unit)?):
  BaseAdapter<HomeContract.EffectViewModel, EffectsAdapter.EffectViewHolder>(effects) {

  override fun bind(holder: EffectViewHolder, viewModel: HomeContract.EffectViewModel) {

    // Find the views
    val effectImageView = holder.itemView.findViewById<ImageView>(R.id.iv_effect_tile_effect)
    val effectTitleView = holder.itemView.findViewById<TextView>(R.id.tv_effect_tile_title)

    // Assign them their values
    effectTitleView.text = viewModel.name

    Glide.with(context)
      .load(viewModel.resourceId)
      .into(effectImageView)

    effectImageView.setOnClickListener { clickListener?.invoke(viewModel) }
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EffectViewHolder {
    return EffectViewHolder(LayoutInflater.from(parent.context)
      .inflate(R.layout.view_effect_tile, parent, false))
  }

  class EffectViewHolder(itemView: View): BaseViewHolder(itemView)
}