package com.alexal.artilizer.home.view

import android.content.Context
import com.alexal.artilizer.R
import com.alexal.artilizer.base.clean_code.BaseActivity
import com.alexal.artilizer.di.DIUtil
import com.alexal.artilizer.home.view.picture_screen.PictureCaptureView
import com.alexal.artilizer.home.view.picture_screen.PictureResultView
import com.alexal.artilizer.home.view.picture_screen.SelectEffectsView
import kotlinx.android.synthetic.main.activity_home.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


class HomeActivity : BaseActivity(), Navigator {

  // Picture screen subviews
  lateinit var pictureCaptureView: PictureCaptureView
  lateinit var pictureEffectsView: SelectEffectsView
  lateinit var pictureResultView: PictureResultView

  val FRAGMENT_CONTAINER = R.id.fl_home_v_container

  override fun attachBaseContext(newBase: Context?) {
    super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
  }

  override fun initViewHandles() {
    initNavigation()
    initSubViews()
    navigateToCapturePictureScreen()
  }

  private fun initNavigation(){
    bnv_home.setOnNavigationItemSelectedListener {
      when(it.itemId){
        R.id.menu_home_picture -> navigateToCapturePictureScreen()
        R.id.menu_home_my_art -> navigateToMyArtScreen()
        R.id.menu_home_settings -> navigateToSettingsScreen()
        else -> true
      }
    }
  }

  private fun initSubViews() {

    // Pictures view
    pictureCaptureView = PictureCaptureView.newInstance()

    // MyArt view

    // Settings view
  }

  override fun getLayoutId(): Int { return R.layout.activity_home }

  override fun navigateToCapturePictureScreen(): Boolean {

    val pictureCapturePresenter = DIUtil.initCapturePresenter(pictureCaptureView, this)

    pictureCaptureView.injectPresenter(pictureCapturePresenter)

    supportFragmentManager.beginTransaction()
      .replace(FRAGMENT_CONTAINER, pictureCaptureView)
      .commit()

    return true
  }

  override fun navigateToPictureResultScreen() {

  }

  override fun navigateToMyArtScreen(): Boolean {
    return true
  }

  override fun navigateToSettingsScreen(): Boolean {
    return true
  }

  override fun navigateToEffectsScreen(): Boolean {
    return true
  }

  override fun navigateToPreview() {

  }

}
