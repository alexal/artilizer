package com.alexal.artilizer.domain.remote

import android.net.Uri

/**
 *
 * @author alexal
 */
data class Effect(private val id: Long,
                  private val imageUri: Uri,
                  private val name: String,
                  private val dateCreated: String)