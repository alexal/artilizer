package com.alexal.artilizer.base.clean_code

import android.arch.lifecycle.ViewModel


/**
 *
 * Created by alexal on 2/10/2018.
 */
class BaseViewModel<V:BaseContract.View, P:BaseContract.Presenter<V>>: ViewModel() {

  private var presenter: P? = null

  fun setPresenter(presenter: P) {

    if (this.presenter == null)
      this.presenter = presenter
  }

  fun getPresenter(): P? {
    return this.presenter
  }

  override fun onCleared() {
    super.onCleared()
    presenter = null
  }
}