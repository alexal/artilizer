package com.alexal.artilizer.base.clean_code

import java.util.concurrent.Executor

/**
 * Abstract class which you can use to extend and implement your own scenario.
 * It provides the basic concurrent mechanism for your use-case (interactor)
 * and some basic state mechanism.
 *
 * @author alexal
 */
abstract class AbstractInteractor(private val executionThread: Executor,
                                  private val mainThread: Executor): Interactor{

  protected var isRunning: Boolean = false
  protected var isCanceled: Boolean = false

  public fun cancel(){
    isRunning = false
    isCanceled = true
  }

  override fun execute() {
    isRunning = true
    isCanceled = false
    executionThread.execute(this)
  }

}