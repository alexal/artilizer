package com.alexal.artilizer.domain.local

import java.util.Date

/**
 * TODO Configure with server team
 * @author alexal
 */
class PictureResult(val id: Long,
                    val image: ByteArray,
                    val name: String,
                    val dateCreated: Date)